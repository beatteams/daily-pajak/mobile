import 'dart:convert';

import 'package:daily_pajak_mobile/pages/homepage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';

class AuthController{
  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  var token = '';
  Future loginUser(BuildContext context)async {
    const url = 'https://api-daily-pajak.beatfraps.com/api/v1/auth/authenticate';
    // const url = 'http://172.20.31.218:10000/api/v1/auth/authenticate';
    var header = <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    };
    var body = jsonEncode({
      'username': usernameController.text,
      'password': passwordController.text,
    });

    var response = await http.post(
        Uri.parse(url),
        headers: header,
        body: body
    );

    if(response.statusCode==200){
      var loginArr = json.decode(response.body);
      SharedPreferences prefs = await SharedPreferences.getInstance();
      await prefs.setString('jwt', loginArr['token']);
      print(prefs.getString('jwt'));
      Fluttertoast.showToast(
          msg: "Login Success",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.blue,
          textColor: Colors.black,
          fontSize: 16.0
      );
      // await Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => MyHomePage()));
    }else{
      Fluttertoast.showToast(
          msg: "Login Failed",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.blue,
          textColor: Colors.black,
          fontSize: 16.0
      );
      print(response.body);
    }

  }
}